SET datestyle = DMY;
insert into kartoteka.clen (osobni_cislo, jmeno, prijmeni, datum_narozeni )
select 
	osobni_cislo::integer, 
	trim(both '''' from krestni) as krestni, 
	trim(both '''' from prijmeni) as prijmeni, 
	den_nar::date
from farpr_import.f_kartoteka;

-- select * from kartoteka.clen;


insert into kartoteka.dar (clen, datum, castka)
select 
	(select 
	 	id 
	 from kartoteka.clen 
	 where clen.osobni_cislo = f_obet.osobni_cislo::int), 
	den::date, 
	castka::float 
from farpr_import.f_obet;

-- select * from kartoteka.dar;

