EXTENSION = kartoteka # the extensions name
MODULEDIR = kartoteka_pg_dir

DATA =	kartoteka--0.1.sql \
	kartoteka--0.1--0.2.sql \
	kartoteka--0.2--0.3.sql

# REGRESS = install_test

# postgres build stuff
PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)
