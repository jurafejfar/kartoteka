ALTER SCHEMA test RENAME TO farpr_import;

CREATE EXTENSION IF NOT EXISTS file_fdw;

CREATE SERVER farpr_csv_export_files
    FOREIGN DATA WRAPPER file_fdw;

-- Table: farpr_import_sql.f_kartoteka

-- DROP foreign TABLE IF EXISTS farpr_import.f_kartoteka;

CREATE foreign TABLE IF NOT EXISTS farpr_import.f_kartoteka
(
    osobni_cislo character varying,
    c_stav character varying,
    psc character varying,
    c_domacnost character varying,
    c_clenstvi character varying,
    osloveni character varying,
    titul_pred character varying,
    krestni character varying,
    prijmeni character varying,
    ulice character varying,
    mesto character varying,
    titul_za character varying,
    den_nar character varying,
    mesto_nar character varying,
    vek character varying,
    vztah character varying,
    povolani character varying,
    mesto_krtu character varying,
    naz_tel_a character varying,
    naz_tel_b character varying,
    naz_tel_c character varying,
    telefon_a character varying,
    telefon_b character varying,
    telefon_c character varying,
    odkud_od character varying,
    kam_do character varying,
    hlas_pravo character varying,
    vyber character varying,
    platce_salaru character varying,
    vyrazan character varying,
    poznamka character varying,
    memo character varying,
    posl_kont_den character varying,
    posl_kont_druh character varying,
    aktualizace character varying,
    rodne character varying,
    vek_akt character varying,
    den_posl_navstevy character varying,
    skupvyber character varying,
    dary character varying,
    den_nar_letos character varying,
    meil character varying,
    letos_nar character varying,
    d_konfirmace character varying,
    d_krtu character varying,
    d_svatby character varying,
    d_clen_od character varying,
    d_clen_do character varying,
    misto_svatby character varying,
    cirkev_krtu character varying,
    misto_konfir character varying,
    ohlasovat_nar character varying,
    dary_plati character varying,
    konfirmace_an character varying,
    osloveni_za character varying,
    c_vyznani character varying,
    cduvod_od character varying,
    cduvod_do character varying,
    zamestnani character varying,
    pohlavi character varying,
    narodnost character varying,
    vsevyber character varying,
    nazevzp character varying,
    letos_nar_nedele character varying,
    cislo_uctu character varying,
    dary_letos character varying,
    dary_loni character varying
)SERVER farpr_csv_export_files
OPTIONS ( filename '/home/vagrant/nfiesta/kartoteka/csv/F_KARTOTEKA.csv', format 'csv', delimiter ';', encoding 'WIN1250', header 'true' );

-- DROP foreign TABLE IF EXISTS farpr_import.f_obet;

CREATE foreign TABLE IF NOT EXISTS farpr_import.f_obet
(
    ID_OBET character varying,
    OSOBNI_CISLO character varying,
    c_DRUH_OBET character varying,
    DEN character varying,
    CASTKA character varying,
    DOKLAD character varying,
    KRESTNI character varying,
    PRIJMENI character varying,
    VYBER character varying,
    ZA_RODINU character varying,
    VSEVYBER character varying,
    TOJSEMJA character varying,
    DRUH_PLATBY character varying
)
SERVER farpr_csv_export_files
OPTIONS ( filename '/home/vagrant/nfiesta/kartoteka/csv/F_OBET.csv', format 'csv', delimiter ';', encoding 'WIN1250', header 'true' );
