with w_data as (
	select 
		clen.prijmeni, 
		clen.jmeno, 
		extract(year from dar.datum) as rok, 
		sum(castka) as sum_castka
	from kartoteka.clen
	join kartoteka.dar on (dar.clen = clen.id)
	group by extract(year from dar.datum), clen.jmeno, clen.prijmeni
	having extract(year from dar.datum) > 2017
	order by clen.prijmeni, clen.jmeno, extract(year from dar.datum)
)
select 
	jmeno, prijmeni, 
	array_agg(rok order by rok), array_agg(sum_castka order by rok) 
from w_data 
group by jmeno, prijmeni
;

with w_data as (
	select 
		clen.prijmeni, 
		clen.jmeno, 
		extract(year from dar.datum) as rok, 
		sum(castka) as sum_castka
	from kartoteka.clen
	join kartoteka.dar on (dar.clen = clen.id)
	group by extract(year from dar.datum), clen.jmeno, clen.prijmeni
	having extract(year from dar.datum) > 2017
	order by clen.prijmeni, clen.jmeno, extract(year from dar.datum)
)
select 
	prijmeni, jmeno,
	sum(sum_castka * (rok = 2022)::int) as sum_castka_22,
	sum(sum_castka * (rok = 2021)::int) as sum_castka_21,
	sum(sum_castka * (rok = 2020)::int) as sum_castka_20
from w_data
group by jmeno, prijmeni
;