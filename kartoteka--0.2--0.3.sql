create schema kartoteka;

--drop table kartoteka.clen;
CREATE TABLE kartoteka.clen (
	id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	osobni_cislo integer not null,
	jmeno varchar not null,
	prijmeni varchar not null,
	datum_narozeni date
);

CREATE TABLE kartoteka.dar (
	id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
	clen int,
	datum date,
	castka float,
	FOREIGN KEY (clen) REFERENCES kartoteka.clen (id)
);